from ImportLibraries import *
from Editor import Editor

class Movie:        
    def getFrames(self, path):
        filename, _ = os.path.splitext(path)
        filename += "-opencv"

        if not os.path.isdir(filename):
            os.mkdir(filename)
        clip = cv2.VideoCapture(path)
        fps = clip.get(cv2.CAP_PROP_FPS)
        savedFramesPerSecond = min(fps, SAVING_FRAMES_PER_SECOND)
        savedFramesDuration = Editor.getDurationOfClip(Editor, clip, savedFramesPerSecond)
        count = 0
        while True:
            isRead, frame = clip.read()
            if not isRead: 
                break
            frameDuration = count / fps
            try: 
                minDuration = savedFramesDuration[0]
            except IndexError: 
                break
            if frameDuration >= minDuration:
                formattedFrameDuration = Editor.changeDataFormat(Editor, timedelta(seconds = frameDuration))
                cv2.imwrite(os.path.join(filename, f"frame{formattedFrameDuration}.jpg"), frame)
                try: 
                    savedFramesDuration.pop(0)
                except IndexError:
                    pass
            count += 1
    
    def cutTheOriginalVideo(self, from_, to_, path):
        ffmpeg_extract_subclip(path, from_, to_, 'cutted.mp4')

    def deleteEqualFrames(self, path):
        count = 0
        Red, Green, Blue = 255, 255, 255
        for filename in os.listdir(path):
            sc = [0, 0, 0]
            count +=1
            img = cv2.imread(os.path.join(path + '/' + filename))
            height, width, _ = img.shape
            size = height * width
            # for i in range(height):
            #     for j in range(width):
            #         sc[0] += img[i, j][0] / size
            #         sc[1] += img[i, j][1] / size
            #         sc[2] += img[i, j][2] / size

            currentRed, currentGreen,  currentBlue = np.sum(img[:, :, 2])/size, np.sum(img[:, :, 1])/size, np.sum(img[:, :, 0])/size
            if abs(Red - currentRed) <= DIFF_RED and abs(Green - currentGreen) <= DIFF_GREEN and abs(Blue - currentBlue) <= DIFF_BLUE:
                os.remove(path + '/' + filename)
                Red, Green, Blue = currentRed, currentGreen, currentBlue
            else:
                Red, Green, Blue = currentRed, currentGreen, currentBlue

    def createNewVideo(self, from_path):
        arrayOfImages = []
        for filename in os.listdir(from_path):
            img = cv2.imread(from_path + '/' + filename)
            height, width, _ = img.shape
            size = (width, height)
            arrayOfImages.append(img)
        out = cv2.VideoWriter('output.avi', cv2.VideoWriter_fourcc(*'XVID'), 3, size)
        for i in range(len(arrayOfImages)):
            out.write(arrayOfImages[i])
        out.release()

    def fromAviToMp4(self, avi_file_path, output_name):
        os.popen("ffmpeg -i {input} -ac 2 -b:v 2000k -c:a aac -c:v libx264 -b:a 160k -vprofile high -bf 0 -strict experimental -f mp4 {output}.mp4".format(input = avi_file_path, output = output_name))

    def concatVideoAndAudio(self):
        os.popen("ffmpeg -i 3495.mp3 -i output.mp4 -codec copy -shortest video_finale.mp4")

    def finishTheWork(self):
        os.popen("shutdown /s")
