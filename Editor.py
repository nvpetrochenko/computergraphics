import cv2
import numpy as np

class Editor:
    def changeDataFormat(self, previousData): 
        result = str(previousData)
        try:
            result, miliseconds = result.split(".")
        except ValueError:
            return result + ".00".replace(":", "-")
        miliseconds = int(miliseconds)
        miliseconds = round(miliseconds / 1e4)
        return f"{result}.{miliseconds:02}".replace(":", "-")

    def getDurationOfClip(self, clip, savedFramesPerSecond):
        seconds = list()
        clipDuration = clip.get(cv2.CAP_PROP_FRAME_COUNT) / clip.get(cv2.CAP_PROP_FPS)
        for i in np.arange(0, clipDuration, 1 / savedFramesPerSecond):
            seconds.append(i)
        return seconds